Rank   Club Name                 POINTS   MATCHES     WONS    LOSSES     DRAWS 
----   -------------             ------   --------   ------   ------     -----
1      New York Rangers            76        22        16         5         1
2      Toronto Maple Leafs         75        22        16         6         0
3      Chicago Blackhawks          68        22        15         6         1
4      Philadelphia Flyers         68        22        14         7         1
5      Anaheim Ducks               68        22        14         8         0
6      Detroit Red Wings           61        22        11         9         2
7      Vancouver Canucks           54        22        11        11         0
8      Boston Bruins               49        22        10        12         0
9      Pittsburgh Penguins         48        22         9        12         1
10     Montreal Canedians          40        22         7        14         1
11     Colarado Avalanche          34        22         5        16         1
12     San Joes Sharks              1        22         0        22         0
