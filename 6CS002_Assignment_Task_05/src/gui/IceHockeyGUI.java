package gui;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import ice_hockey.IceHockey01;
import ice_hockey.IceHockey02;
import ice_hockey.IceHockey03;
import ice_hockey.IceHockey04;
import ice_hockey.IceHockey05;

/*************************************
 * This class demonstrates GUI for Ice Hockey classes
 * 
 * 
 * @author Chaniru Puldith, UoW ID: 2220488
 * ***********************************/



public class IceHockeyGUI extends JFrame {
private JPanel contentPane;
	
	public IceHockeyGUI() {
		setResizable(false);
		setTitle("~6CS002 Task 05: 2220488~");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(400, 400);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBackground(Color.DARK_GRAY);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel title = new JLabel("<html><h1><strong><i>ICE HOCKEY</i></strong></h1><hr></html>");
		title.setBounds(120, 20, 45, 16);
		title.resize(300, 50);
		title.setForeground(Color.cyan);
		contentPane.add(title);
		
		JButton iceHockeyBtn01 = new JButton("Get Details");
		iceHockeyBtn01.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				IceHockey01.main(null);
			}
		});
		iceHockeyBtn01.setBounds(40, 80, 120, 29);
		iceHockeyBtn01.resize(140, 40);
		contentPane.add(iceHockeyBtn01);
		
		JButton iceHockeyBtn02 = new JButton("Compare Streams");
		iceHockeyBtn02.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				IceHockey02.main(null);
			}
		});
		iceHockeyBtn02.setBounds(210, 80, 120, 29);
		iceHockeyBtn02.resize(140, 40);
		contentPane.add(iceHockeyBtn02);
		
		JButton iceHockeyBtn03 = new JButton("League Summary");
		iceHockeyBtn03.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				IceHockey03.main(null);
			}
		});
		iceHockeyBtn03.setBounds(40, 140, 117, 29);
		iceHockeyBtn03.resize(140, 40);
		contentPane.add(iceHockeyBtn03);
		
		JButton iceHockeyBtn04 = new JButton("Club Summary");
		iceHockeyBtn04.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				IceHockey04.main(null);
			}
		});
		iceHockeyBtn04.setBounds(210, 140, 117, 29);
		iceHockeyBtn04.resize(140, 40);
		contentPane.add(iceHockeyBtn04);
		
		JButton iceHockeyBtn05 = new JButton("Sort Clubs");
		iceHockeyBtn05.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				IceHockey05.main(null);
			}
		});
		iceHockeyBtn05.setBounds(120, 200, 117, 29);
		iceHockeyBtn05.resize(140, 40);
		contentPane.add(iceHockeyBtn05);
		
		JButton exit = new JButton("Exit !");
		exit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		exit.setBounds(280, 310, 117, 29);
		exit.resize(80, 30);
		exit.setBackground(Color.LIGHT_GRAY);
		contentPane.add(exit);
	}
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					IceHockeyGUI frame = new IceHockeyGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
