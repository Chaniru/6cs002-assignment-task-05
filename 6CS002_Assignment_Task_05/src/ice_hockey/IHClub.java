package ice_hockey;

/*************************************
 * This class stores the details of a Ice Hockey clubs in USA
 * including the performance measures that determine their position 
 * in the league.The class implements the Comparable interface,
 * which determines how clubs will be sorted.
 * 
 * 
 * @author Chaniru Puldith, UoW ID: 2220488
 * ***********************************/

public class IHClub implements Comparable<IHClub>{
	  private int rank; // Club Rank
	  private String clubName; // Club Name
	  private int totalMatches; // Total match count played by the club
	  private int totalWons; // Total win count played matches by the club
	  private int totalDraws; // Total draw count played matches by the club
	  private int totalLosses; // Total lost count played matches by the club
	  private int pointsGained; // Total points achieved by the club
	  private int pointsLost; // Total points lost by the club
	  private int pointsDifference; // Difference of gained and lost points
	  private int goalsGained; // Total goals achieved by the club
	  private int goalsAgainst; // Total goals achieved against the club
	  private int assistBonus; // Total bonus points achieved for assists
	  private int losingBonus; // Total goals achieved for losing a match
	  private int clubPoints; // Total ranking points

	  public IHClub(int rank, String clubName, int totalMatches, int totalWons, int totalDraws,
	      int totalLosses, int pointsGained, int pointsLost, int pointsDifference,
	      int goalsGained, int goalsAgainst, int assistBonus, int losingBonus,
	      int clubPoints) {
	    this.rank = rank;
	    this.clubName = clubName;
	    this.totalMatches = totalMatches;
	    this.totalWons = totalWons;
	    this.totalDraws = totalDraws;
	    this.totalLosses = totalLosses;
	    this.pointsGained = pointsGained;
	    this.pointsLost = pointsLost;
	    this.pointsDifference = pointsDifference;
	    this.goalsGained = goalsGained;
	    this.goalsAgainst = goalsAgainst;
	    this.assistBonus = assistBonus;
	    this.losingBonus = losingBonus;
	    this.clubPoints = clubPoints;
	  }

	  public String toString() {
	    return String.format("%-7d%-20s%10d%10d%10d%10d%10d", this.rank, this.clubName, this.clubPoints, this.totalMatches,
	        this.totalWons, this.totalLosses, this.totalDraws);
	  }

	  public int getRank() {
	    return rank;
	  }

	  public void setRank(int rank) {
	    this.rank = rank;
	  }

	  public String getClubName() {
	    return clubName;
	  }

	  public void setClubName(String club) {
	    this.clubName = club;
	  }

	  public int getTotalMatches() {
	    return totalMatches;
	  }

	  public void setTotalMatches(int totalMatches) {
	    this.totalMatches = totalMatches;
	  }

	  public int getTotalWons() {
	    return totalWons;
	  }

	  public void setTotalWons(int totalWons) {
	    this.totalWons = totalWons;
	  }

	  public int getTotalDraws() {
	    return totalDraws;
	  }

	  public void setTotalDraws(int totalDraws) {
	    this.totalDraws = totalDraws;
	  }

	  public int getTotalLosses() {
	    return totalLosses;
	  }

	  public void setTotalLosses(int totalLosses) {
	    this.totalLosses = totalLosses;
	  }

	  public int getPointsGained() {
	    return pointsGained;
	  }

	  public void setPointsGained(int pointsGained) {
	    this.pointsGained = pointsGained;
	  }

	  public int getPointsLost() {
	    return pointsLost;
	  }

	  public void setPointsLossed(int pointsLossed) {
	    this.pointsLost = pointsLossed;
	  }

	  public int getPointsDifference() {
	    return pointsDifference;
	  }

	  public void setPointsDifference(int pointsDifference) {
	    this.pointsDifference = pointsDifference;
	  }

	  public int getGoalsGained() {
	    return goalsGained;
	  }

	  public void setGoalsGained(int goalsGained) {
	    this.goalsGained = goalsGained;
	  }

	  public int getGoalsAgainst() {
	    return goalsAgainst;
	  }

	  public void setGoalsAgainst(int goalsAgainst) {
	    this.goalsAgainst = goalsAgainst;
	  }

	  public int getAssistBonus() {
	    return assistBonus;
	  }

	  public void setAssistBonus(int assistBonus) {
	    this.assistBonus = assistBonus;
	  }

	  public int getLosingBonus() {
	    return losingBonus;
	  }

	  public void setLosingBonus(int losingBonus) {
	    this.losingBonus = losingBonus;
	  }

	  public int getPoints() {
	    return clubPoints;
	  }

	  public void setPoints(int points) {
	    this.clubPoints = points;
	  }
	  
	  public int compareTo(IHClub clubIH) {
	    return ((Integer) clubPoints).compareTo(clubIH.clubPoints);
	  }

}
