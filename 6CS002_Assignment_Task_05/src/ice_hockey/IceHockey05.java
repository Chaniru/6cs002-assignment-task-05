package ice_hockey;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.OptionalInt;

/*************************************
 * This example demonstrates processing a list of integers via 
 * use of sorting   
 * 
 * @author Chaniru Puldith, UoW ID: 2220488
 * ***********************************/

public class IceHockey05 {
  public static void main(String[] args) {
	  List<IHClub> table = Arrays.asList(
	    		new IHClub(1, "New York Rangers", 22, 16, 1, 5, 621, 400, 221, 75, 41, 8, 2, 76),
	            new IHClub(2, "Toronto Maple Leafs", 22, 16, 0, 6, 625, 414, 211, 72, 43, 9, 2, 75),
	            new IHClub(3, "Chicago Blackhawks", 22, 15, 1, 6, 453, 421, 32, 37, 39, 4, 2, 68),
	            new IHClub(4, "Philadelphia Flyers", 22, 14, 1, 7, 664, 418, 246, 70, 40, 5, 5, 68),
	            new IHClub(5, "Anaheim Ducks", 22, 14, 0, 8, 663, 437, 226, 70, 46, 5, 7, 68),
	            new IHClub(6, "Detroit Red Wings", 22, 11, 2, 9, 672, 527, 145, 77, 54, 9, 4, 61),
	            new IHClub(7, "Vancouver Canucks", 22, 11, 0, 11, 497, 482, 15, 62, 54, 6, 4, 54),
	            new IHClub(8, "Boston Bruins", 22, 10, 0, 12, 444, 514, -70, 45, 50, 4, 5, 49),
	            new IHClub(9, "Pittsburgh Penguins", 22, 9, 1, 12, 553, 575, -22, 53, 61, 4, 6, 48),
	            new IHClub(10, "Montreal Canedians", 22, 7, 1, 14, 442, 578, -136, 46, 57, 4, 6, 40),
	            new IHClub(11, "Colarado Avalanche", 22, 5, 1, 16, 475, 545, -70, 57, 61, 4, 8, 34),
	            new IHClub(12, "San Joes Sharks", 22, 0, 0, 22, 223, 1021, -798, 29, 147, 1, 0, 1));
		     
	// Printing club details sorted by Comparator in IHClub class in Ascending order 
    System.out.println("Sorted by Comparator in IHClub class (by Club Points) (Order: ASC.) :-");
    System.out.printf("%-7s%-22s%10s%10s%9s%10s%10s \n\n", "Rank", "Club Name", "POINTS", "MATCHES", "WONS", "LOSSES", "DRAWS");
    table.stream().sorted().forEach(System.out::println);
    
    // Printing club details sorted by lambda by Total Draws in Descending order 
    System.out.println();
    System.out.println("\nSorted by lambda by Total Draws (Order: DESC.) :-");
    System.out.printf("%-7s%-22s%10s%10s%9s%10s%10s \n\n", "Rank", "Club Name", "POINTS", "MATCHES", "WONS", "LOSSES", "DRAWS");
    table.stream()
         .sorted((c1, c2) -> 
            ((Integer) c2.getTotalDraws()).compareTo(c1.getTotalDraws()))
         .forEach(System.out::println);

  
  try {
      FileWriter writer = new FileWriter("IH_Report5.txt");
      
      // Writing club details sorted by lambda by Total Draws in Descending order to IH Report5
      writer.write("Sorted by Comparator in IHClub class (by Points ASC.) :-\n");
      writer.write(String.format("%-7s%-22s%10s%10s%9s%10s%10s \n", "Rank", "Club Name", "POINTS", "MATCHES", "WONS", "LOSSES", "DRAWS"));
      writer.write("----   -------------             ------   --------   ------   ------     -----\n");
      table.stream().sorted()
        .forEach(str -> {
        	try {
        		writer.write(str.toString() + "\n");
			} catch (IOException e) {
				e.printStackTrace();
			}
        });
      
      // Writing club details sorted by lambda by Total Draws in Descending order to IH Report5
      writer.write("\n\nSorted by lambda by Total Draws (Order: DESC.) :-\n");
      writer.write(String.format("%-7s%-22s%10s%10s%9s%10s%10s \n", "Rank", "Club Name", "POINTS", "MATCHES", "WONS", "LOSSES", "DRAWS"));
      writer.write("----   -------------             ------   --------   ------   ------     -----\n");
      table.stream().sorted((c1, c2) -> 
        ((Integer) c2.getTotalDraws()).compareTo(c1.getTotalDraws()))
        .forEach(str -> {
        	try {
        		writer.write(str.toString() + "\n");
			} catch (IOException e) {
				e.printStackTrace();
			}
        });
      
      writer.close();
      System.out.println("\nIH_Report5.txt file successfully written.\n\n");
    } catch (IOException e) {
      System.out.println("An error has occurred.");
      e.printStackTrace();
    }
}


}
